
#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('c');
	mylist.pushToTail('o');
	mylist.pushToHead('n');
	mylist.pushToHead('e');
	mylist.pushToTail('s');
	mylist.pushToHead('k');
	mylist.pushToTail('h');
	mylist.print();
	
	for (int i = 0; i < 3; i++)
	{
		cout << "\ntesting pop head :" << mylist.popHead();
		cout << "\ntesting pop tail :" << mylist.popTail() << endl;
		cout << "print again : ";
		mylist.print();
		cout << "\nSearching for n ";
		if (mylist.search('n'))
		{
			cout << "\nIt's here";
		}
		else { cout << "It's doesn't in this list"; }
		cout << endl;
	}
	cout << "\ntesting pop head :" << mylist.popHead(); cout << "\ntesting pop head :" << mylist.popHead();
	system("pause");
	
	//TO DO! Complete the functions, then write a program to test them.
	//Adapt your code so that your list can store data other than characters - how about a template?

}