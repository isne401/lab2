#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}
void List::pushToHead(char el)
{
	head = new Node(el, head, 0);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	
	if (head == 0)
	{
		Node *tmp = new Node(el, 0, 0);
		head = tmp;
		tail = tmp;
	}
	else
	{
		Node *tmp = new Node(el, 0, tail);
		tail->next = tmp;
		tail = tmp;
	}

}
char List::popHead()
{
	char el;
	Node *tmp = head;
	if (head == tail)
	{
		if (head == 0)
			return NULL;  //chang a bit for if nothing is the list
		el = head->data;
		head = tail = 0;
	}
	else
	{
		el = head->data;
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node *tmp = tail;
	char el;
	if (head == tail)
		{
		if (head == 0)
			return NULL;
		head = tail = 0;
		char el = tail->data;
		}
	else {
		el = tail->data;
		tail = tail -> prev;
		tail->next = 0;
		}
	delete tmp;
	return el;
}
bool List::search(char el)
{
	Node *tmp=tail;
	if (tail == head)
	{
		if (head == 0)
			return false;
		if (el == head->data)
			return true;
		else return false;
	}
	while (tmp->prev!=0)
	{
		if (el == tmp->data)
			return true;
		tmp = tmp->prev;
	}
	return false;
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}